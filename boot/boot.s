/*
 boot.s
 työntää cstring sarjaporttiin
 
        arm-none-eabi-as -mcpu=arm926ej-s -g boot.s -o boot.o 
        arm-none-eabi-ld -T boot.ld boot.o -o boot.elf 
        arm-none-eabi-objcopy -O binary boot.elf boot.bin

 	qemu-system-arm -M versatilepb -m 512M -kernel boot.bin -monitor stdio

 boot.ld file
 
 ENTRY(_reset)
 SECTIONS
 {
  . = 0x10000;
  .startup . : { boot.o(.text) }
  .text : { *(.text) }
  .data : { *(.data) }
  .bss : { *(.bss COMMON) }
  . = ALIGN(8);
  . = . + 0x1000; [> 4kB of stack memory <]
  stack_top = .;
 }

*/


.global _reset
.set UART0DR, 0x101f1000

_reset:
	ldr sp, =stack_top

main:
	adr r1, msg_test
	bl println
	b .

println:
	ldr r10, =UART0DR
.loop:
	ldrb r0, [r1], #1
	cmp r0, #0
	beq .done

	/*
	 *survotaan yksi char UART0DR
	 *ilmeisesti real hardwaressa tarvii
	 *väijyä onko UART buffer täynnä
	 */

	str r0, [r10]
	b .loop
.done:
	mov pc, lr

msg_test: .asciz "The ride never ends.\n\r"
